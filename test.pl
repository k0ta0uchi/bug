use utf8;
use warnings;
use strict;

our %BUGGYHASH = (
		'fizz' => 'bug',
	);


while(1){
	print "fizz: ".&get_bugs("the type of bug is fizz.")."\n";
}

sub get_bugs {
	my $type = shift;
	while ( my ($key, $value) = each %BUGGYHASH ) {
		if($type =~ /${key}/i){
			return $value;
		}
	}
	return "";
}
